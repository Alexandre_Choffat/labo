class LaboException(Exception):
    """ Généralise les exceptions du laboratoire."""
    pass


class AbsentException(LaboException):
    pass


class PresenceException(LaboException):
    pass

def creer_labo():
    """
    Créer un nouveau laboratoire

    """
    lab = {}
    return(lab)

def enregistrer_arrivee(labo,nom,bureau):
    """
    Enregistrer l'arrivée d'une personne
    """

    if nom in labo:
        raise PresenceException(nom)
    
    labo[nom] = bureau

def enregistrer_depart(labo,nom):
    """
    Enregistrer le départ d'une personne
    """
    if nom not in labo :
        raise AbsentException(nom)
    del labo[nom]
    

def modifier_bureau(labo,nom,nouveau_bureau):
    '''
    Modifier le bureau d'une personne
    '''
    if nom not in labo :
        raise AbsentException(nom)
    labo[nom] = nouveau_bureau
    

def changer_nom(labo,nom,nouveau_nom):
    '''
    Changer le nom d'une personne
    '''
    if nom not in labo :
        raise AbsentException(nom)
    if nouveau_nom in labo :
        raise PresenceException(nouveau_nom)
    bureau = labo[nom]
    del labo[nom]
    labo[nouveau_nom] = bureau

def presence(labo,nom):
    '''
    Savoir si une personne est présente
    '''
    return(nom in labo)


def obtenir_bureau(labo,nom):
    '''
    Obtenir le bureau de nom.
    '''
    if nom not in labo : 
        raise AbsentException(nom)
    bureau = labo[nom]
    return(bureau)
    

def listing(labo):
    '''
    Obtenir le listing du personnel
    '''
    for nom in labo.keys():
        print (f'{nom} : {labo[nom]}')
    
def occupation_bureaux(labo):
    '''
    Obtenir l'occupation des bureaux sous forme de texte.
    '''
    items = labo.items()
    sorted_items = sorted (items,key = lambda (x,y) : y)
        last_salle = ""
        for (nom,salle) in sorted_items :
            if salle != last_salle :
                print(f'{salle}:')
            print(f)