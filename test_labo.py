import labo
import pytest

#Test de la création de laboratoire.
def test_creation_lab():
    lab = labo.creer_labo()
    assert( lab == {})

#Test de l'enregistrement.
def test_enregistrement():
    lab = labo.creer_labo()
    labo.enregistrer_arrivee(lab, 'Xavier', 'F305')
    assert 'Xavier' in lab
    assert lab['Xavier'] == 'F305'

#Test du départ.
def test_depart():
    lab = labo.creer_labo()
    labo.enregistrer_arrivee(lab, 'Xavier', 'F305')
    labo.enregistrer_depart(lab,'Xavier')
    assert 'Xavier' not in lab

#Test de modification de bureau
def test_modif():
    lab = labo.creer_labo()
    labo.enregistrer_arrivee(lab,'Xavier','F305')
    labo.modifier_bureau(lab,'Xavier','F256')
    assert lab['Xavier'] == 'F256'

#Test de changement de nom
def test_chgt_nom():
    lab = labo.creer_labo()
    labo.enregistrer_arrivee(lab,'Xavier','F305')
    labo.changer_nom(lab,'Xavier','Ariane')
    assert 'Ariane' in lab
    assert 'Xavier' not in lab
    assert lab['Ariane'] == 'F305'

#Test de présence
def test_presence():
    lab = labo.creer_labo()
    labo.enregistrer_arrivee(lab,'Xavier','F305')
    labo.changer_nom(lab,'Xavier','Ariane')
    assert labo.presence(lab,'Ariane')
    assert not labo.presence(lab,'Xavier')

#Test d'obtenir_bureau
def test_obt_bureau():
    lab = labo.creer_labo()
    labo.enregistrer_arrivee(lab,'Vivien','F367')
    assert labo.obtenir_bureau(lab,'Vivien') == 'F367'