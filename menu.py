def nouveau_menu():
    menu = [("Quitter le programme",None)]
    return(menu)

def ajouter_option(menu,description,fonction):
    menu.append((description,fonction))

def afficher(menu):
    #Afficher successivement les éléments de menu.
    for (i,(description,fonction)) in enumerate(menu):
        print(f'{i} : {description}')

def traiter_choix (menu,choix):
    #On récupère la fonction associée au choix.
    description,fonction = menu[choix]
    #On l'execute.
    fonction()



