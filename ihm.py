import labo
import menu

def ihm():
    #Initialiser le labo.
    lab = labo.creer_labo()
    #Afficher menu
    print('Bienvenue dans la gestion de laboratoire')
    choix = -1
    #Menu initialisé avec l'option pour quitter.
    m = menu.nouveau_menu()
    #Option1
    menu.ajouter_option(m,"Enregistrer l'arrivée d'une nouvelle personne.",gerer_arrivee(lab))
    #Option2
    menu.ajouter_option(m,"Enregistrer le départ d'une personne.",gerer_depart(lab))
    #Option3
    menu.ajouter_option(m,"Modifier le bureau occupé par une personne.",gerer_modif_bureau(lab))
    #Option4
    menu.ajouter_option(m,"Changer le nom d'une personne.",gerer_changer_nom(lab))
    #Option5
    menu.ajouter_option(m,"Savoir si un personne est membre.",gerer_presence(lab))
    #Option6
    menu.ajouter_option(m,"Obtenir le bureau d'une personne.",gerer_obt_bureau(lab))
    #Option7
    menu.ajouter_option(m,"Obtenir un listing du personnel.",gerer_listing(lab))
    #Boucle de traitement des requêtes.
    while choix != 0 : 
        #Affichage du menu.
        print('Voici les différentes options :')
        #Afficher les choix
        menu.afficher(m)
        choix = int(input('Que voulez vous faire ? '))
        #Afficher un message de fin le cas échéant.
        if choix == 0:
            print('Au revoir')
        #Utiliser le menu pour la redirection vers la bonne fonction.
        elif choix < len(m):
            try :
                menu.traiter_choix(m,choix)
            except labo.PresenceException:
                print("La personne est déjà présente")
            except labo.AbsentException:
                print("Personne de ce nom dans le laboratoire.")
        #Traiter un input erroné de l'utilisateur.
        else :
            print("Je n'ai pas compris votre choix.")

def gerer_arrivee(lab):

    def local(): 
        nom = input("Comment s'appelle la nouvelle personne ? ")
        bureau = input("Quel est son bureau ?")
        labo.enregistrer_arrivee(lab,nom,bureau)
    return(local)

def gerer_depart(lab):
    def local():
        nom = input("Comment s'appelle la personne qui part? ")
        labo.enregistrer_depart(lab,nom)
    return(local)

def gerer_modif_bureau(lab):
    def local():
        nom = input("Comment s'appelle la personne dont vous voulez modifier le bureau ? ")
        nouveau_bureau = input("Quel est son nouveau bureau ? ")
        labo.modifier_bureau(lab,nom,nouveau_bureau)
    return(local)

def gerer_changer_nom(lab):
    def local():
        nom = input("Quel est le nom que vous voulez modifier ? ")
        nouveau_nom = input("Quel est le nouveau nom de cette personne ? ")
        labo.changer_nom(lab,nom,nouveau_nom)
    return(local)

def gerer_presence(lab):
    def local():
        nom = input('Quel est le nom de cette personne ?')
        if labo.presence(lab,nom):
            print("Cette personne est membre.")
        else : 
            print("Cette personne n'est pas membre.")
    return(local)

def gerer_obt_bureau(lab):
    def local():
        nom = input("Quel est le nom de cette personne ? ")
        labo.obtenir_bureau(lab,nom)
        print(f' Son bureau est {bureau}')
    return(local)

def gerer_listing(lab):
    def local():
        labo.listing(lab)
    return(local)

def gerer_occupation(lab):
    def local():
        labo.occupation_bureaux(lab)
    return(local)

ihm()

